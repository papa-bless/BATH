from dotenv import load_dotenv

from bath.tub.db_controller import DBController
from bath.tub.model_tweet import Tweet
from bath.tub.model_hashtag import Hashtag
from bath.tub.model_mention import Mention
from bath.tub.model_place import Place

load_dotenv()


def test_create_tweet():
    d = DBController()
    session = d.get_session()

    t = Tweet(
        tweet_id="testID", text="this is a test tweet", user_id="testUser1"
    )
    t.hashtags.append(Hashtag(hashtag_id="testHashtag"))
    t.mentions.append(Mention(user_id="testUser2"))
    session.add(t)

    q = session.query(Tweet).filter_by(tweet_id="testID").first()
    session.rollback()
    assert q == t


def test_create_place():
    d = DBController()
    session = d.get_session()

    p = Place(
        place_id="place1",
        url="http://url.com",
        place_type="city",
        name="Tulsa",
        full_name="Tulsa, Ok",
        country_code="US",
        bounding_box="POLYGON((0 0,1 0,1 1,0 1,0 0))",
        tweet_count=1,
        avg_sentiment=2.5,
    )
    session.add(p)
    q = session.query(Place).filter_by(place_id="place1").first()
    session.rollback()
    assert q == p
