import argparse
import json
import socketio

remotes = {
    "local": "http://localhost:5000",
    "staging": "https://bath.howyoufeel.today:5001",
    "prod": "https://bath.howyoufeel.today:5000",
}

parser = argparse.ArgumentParser()
parser.add_argument("host", type=str, default="prod", nargs="?")
args = parser.parse_args()


HOST = remotes[args.host]

sio = socketio.Client()


@sio.event
def connect():
    print("connected to host {}".format(HOST))


@sio.event
def disconnect():
    print("disconnected")


@sio.on("tweet-place")
def handle_place(data):
    data = json.loads(data)
    print(data)


@sio.on("tweet-coordinates")
def handle_coordinates(data):
    data = json.loads(data)
    print(data)


@sio.on("history")
def history(data):
    data = json.loads(data)
    for item in data:
        print(item)


def motd():
    f = open("motd").read()
    for line in f.splitlines():
        print(line)
    print("-" * 43)
    print("    {}".format(HOST))
    print("-" * 43)


if __name__ == "__main__":
    motd()
    try:
        sio.connect(HOST)
    except socketio.exceptions.ConnectionError:
        print("unable to connect")
