import catboost
import re
import os
from textblob import TextBlob


class Model:
    def __init__(self):
        self.model = catboost.CatBoostClassifier()
        self.fname = os.path.abspath("bath/data/catboost_model.dump")
        self.model.load_model(self.fname)

    def clean_tweet(self, text):
        return " ".join(
            re.sub(
                r"(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])|(\w+:\/\/\S+)", " ", text
            ).split()
        )

    def score_tweet(self, text):
        cleaned_tweet = self.clean_tweet(text)
        blob_tweet = TextBlob(cleaned_tweet)

        return self.model.predict(
            [
                len(blob_tweet.words),
                len(blob_tweet.word_counts),
                blob_tweet.sentiment[0],
                blob_tweet.sentiment[1],
            ]
        )
