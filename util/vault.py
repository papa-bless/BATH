import json
import os

import hvac
from google.cloud import storage


class Vault:
    def __init__(self, TOKEN, **kwargs):
        self.logger = kwargs.get("logger", None)
        self.twitter_path = kwargs.get("twitter_path", "twitter")
        self.gcp_path = kwargs.get("gcp_path", None)
        self.client = hvac.Client(
            url="https://vault.howyoufeel.today:8200", token=TOKEN
        )
        self.logger.info("Vault initialized")

        self.twitter_keys = None
        self.get_twitter_keys()
        if self.gcp_path:
            self.create_gcp_keyfile()

    def get_twitter_keys(self):
        if self.twitter_keys is not None:
            return self.twitter_keys
        else:
            if self.client.is_authenticated():
                self.logger.info("twitter API keys: requesting...")
                self.twitter_keys = self.client.secrets.kv.v2.read_secret_version(
                    path=self.twitter_path, mount_point="kv"
                )[
                    "data"
                ][
                    "data"
                ]
                self.logger.info("twitter API keys: success")
                return self.twitter_keys

    def create_gcp_keyfile(self, **kwargs):
        force_create = kwargs.get("force_create", False)
        fname = kwargs.get("fname", "keyfile.json")

        if not force_create and os.path.exists(fname):
            self.logger.info("keyfile exists")
            return

        elif self.client.is_authenticated():
            self.logger.info("gcp auth: requesting...")
            gcp_auth = self.client.secrets.kv.v2.read_secret_version(
                path=self.gcp_path, mount_point="kv"
            )["data"]["data"]
            self.logger.info("gcp auth: success")
            self.gcp_values = dict(
                type="service_account",
                project_id="tulsahowyoufeel",
                private_key_id=gcp_auth["private_key_id"],
                private_key=gcp_auth["private_key"].replace("\\n", "\n"),
                client_email=gcp_auth["client_email"],
                client_id=gcp_auth["client_id"],
                auth_uri=gcp_auth["auth_uri"],
                token_uri=gcp_auth["token_uri"],
                auth_provider_x509_cert_url=gcp_auth[
                    "auth_provider_x509_cert_url"
                ],
                client_x509_cert_url=gcp_auth["client_x509_cert_url"],
            )

            f = open(fname, "w")
            f.write(json.dumps(self.gcp_values, indent=2))
            f.write("\n")
            self.logger.info("created keyfile: {}".format(fname))
