import os
from shapely.geometry import Polygon, Point

import util.common as common


class Tulsa:
    def __init__(self, logger=None):
        self.logger = logger
        self.polygon = Polygon(self.get_coords())
        self.logger.info("polygon created")

    def get_coords(self):

        with open(common.repo_root("data/tulsa_county.txt")) as file:
            data = file.read()
            coord_strings = data
            self.logger.info("coordinate file loaded")

        # remove any trailing whitespace
        coord_strings = coord_strings.strip("\n\t")

        # remove extraneous spaces to allow for float parsing
        coord_strings = coord_strings.replace(" - ", "-")

        # not keeping elevation values, so might as well split on them...
        coord_strings = coord_strings.split(", 0")

        # remove empty last element (I know this is all very hacky)
        coord_strings = coord_strings[:-1]

        # maps list of strings into list of [x,y] float pairs
        coords = list(
            map(
                lambda x: [float(x.split(",")[0]), float(x.split(",")[1])],
                coord_strings,
            )
        )
        return coords

    def contains(self, lon, lat):
        return self.polygon.contains(Point(lon, lat))
