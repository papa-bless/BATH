from pathlib import Path
import os


def repo_root(str=None):
    return os.path.join(Path(__file__).parent.parent, (str or ""))


def bath_root(str=None):
    return os.path.join(repo_root(), "bath", (str or ""))


def faucet_root(str=None):
    return os.path.join(repo_root(), "faucet", (str or ""))
