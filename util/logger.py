import os
from datetime import datetime

import util.common as common
import logging.config


class Logger:
    def __init__(self, configfile, prefix=None, level="INFO"):
        logging.config.fileConfig(common.repo_root(configfile))
        self.logger = logging.getLogger("MainLogger")
        self.loglevels = {
            "DEBUG": logging.DEBUG,
            "INFO": logging.INFO,
            "WARNING": logging.WARNING,
            "ERROR": logging.ERROR,
        }

        if prefix:
            self.add_fileHandler(prefix, level)

    def add_fileHandler(self, prefix="BATH", level="INFO"):
        logfile_dir = common.repo_root("log")

        if not os.path.exists(logfile_dir):
            os.makedirs(logfile_dir)

        logfile = common.repo_root(
            "log/{}-{:%Y-%m-%d}.log".format((prefix), datetime.now())
        )

        fh = logging.FileHandler(logfile)
        fh.setLevel(self.loglevels[level])
        formatter = logging.Formatter(
            "%(asctime)s | %(levelname)-8s | %(filename)s-%(funcName)s-%(lineno)04d | %(message)s"
        )
        fh.setFormatter(formatter)
        self.logger.addHandler(fh)
        self.logger.info("log file directory: {}".format(logfile_dir))

    def get_logger(self):
        return self.logger
