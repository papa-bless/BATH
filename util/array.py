class CircularArray:
    """Circular Array"""

    def __init__(self, size):
        assert isinstance(size, int)
        assert size > 0
        self.size = size
        self.array = [None] * size
        self.index = 0
        self.length = 0

    def __getitem__(self, index):
        """override [] operator"""
        assert isinstance(index, int)
        return self.array[index]

    def __len__(self):
        """overide len()"""
        return self.length

    def _index(self):
        """increments self.index"""
        self.index = (self.index + 1) % self.size

    def insert(self, item):
        """inserts an item, overwriting oldest entry"""
        self.array[self.index] = item
        self._index()
        self.length = min(self.length + 1, self.size)

    def items(self):
        """returns generator for iterating through items if item exists"""
        if self.array[self.index] is not None:
            for i in range(self.size):
                yield self.array[self.index]
                self._index()
        else:
            end = self.index
            self.index = 0
            for i in range(end):
                yield self.array[self.index]
                self._index()
