import sched
import datetime
import threading
import time


class Scheduler:
    def __init__(self):
        self.scheduler = sched.scheduler(time.time, time.sleep)

    def add(self, interval, action, actionargs=()):
        action(*actionargs)
        self.scheduler.enter(
            interval, 1, self.add, (interval, action, actionargs)
        )

    def run(self):
        class ScheduleThread(threading.Thread):
            @classmethod
            def run(cls):
                self.scheduler.run()

        continuous_thread = ScheduleThread()
        continuous_thread.start()


if __name__ == "__main__":

    def print_msg():
        print(datetime.datetime.now(), "test")

    s = Scheduler()
    s.add(5, print_msg)
    s.run()
