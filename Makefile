install: poetry.lock
	poetry install
	poetry run pre-commit install
	git config --bool flake8.strict true

test: install tests/
	poetry run pytest -v

lock: poetry.lock
	poetry export -f requirements.txt --dev --without-hashes > requirements.txt

up: lock docker-compose.yml Dockerfile
	docker-compose build && docker-compose up -d redis web && docker-compose logs -f web

down: docker-compose.yml Dockerfile
	docker-compose down

migrate: alembic.ini
	poetry run alembic upgrade head
	poetry run alembic downgrade base
	poetry run alembic upgrade head
