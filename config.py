class BaseConfig(object):
    DEBUG = False
    TESTING = False
    HISTORY = True
    BATH_ENV = "prod"
    BATH_VERSION = "0.5.3"
    VAULT_TWITTER_PATH = "twitter"
    VAULT_GCP_PATH = "gcp-storage"
    STORAGE_SIZE = 1000
    STORAGE_INTERVAL = 300
    STORAGE_DIR = "data"
    STORAGE_BUCKET = "how-you-feel-historical"
    NUM_THREADS = 2
    QUEUE_ADDR = "redis://redis"
    SOCKET_HOST = "0.0.0.0"
    SOCKET_PORT = 5000
    KEYFILE = None
    CERTFILE = None


class ProductionConfig(BaseConfig):
    FLASK_ENV = "production"
    KEYFILE = "certs/privkey1.pem"
    CERTFILE = "certs/fullchain1.pem"
    STORAGE_FILE = "storage-production.pkl"


class StagingConfig(BaseConfig):
    FLASK_ENV = "production"
    KEYFILE = "certs/privkey1.pem"
    CERTFILE = "certs/fullchain1.pem"
    SOCKET_PORT = 5001
    STORAGE_FILE = "storage-staging.pkl"


class DevelopmentConfig(BaseConfig):
    FLASK_ENV = "production"
    BATH_ENV = "dev"
    VAULT_TWITTER_PATH = "twitter-testing"
    VAULT_GCP_PATH = None
    STORAGE_FILE = "storage-development.pkl"


class DebugConfig(BaseConfig):
    FLASK_ENV = "development"
    BATH_ENV = "dev"
    DEBUG = True
    TESTING = True
