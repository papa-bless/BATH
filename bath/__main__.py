from gevent import monkey

monkey.patch_all()

import os
import signal
from multiprocessing import Process, Queue

from flask import Flask

from bath.drain.socket import Socket
from bath.faucet.stream import Stream
from bath.tub.storage import Storage
from dotenv import load_dotenv
from util.coords import Tulsa
from util.logger import Logger
from util.vault import Vault


configs = {
    "production": "config.ProductionConfig",
    "staging": "config.StagingConfig",
    "development": "config.DevelopmentConfig",
    "debug": "config.DebugConfig",
}

# source values in .env
load_dotenv()
BATH_ENV = os.getenv("BATH_ENV", "production")
VAULT_TOKEN = os.getenv("VAULT_TOKEN")

motd = open("motd").read()
for line in motd.splitlines():
    print(line)
print("    Environment: {}".format(BATH_ENV))
print("-" * 43)


# init app,  load appropriate config
app = Flask(__name__)
app.config.from_object(configs[BATH_ENV])

# init utils
logger = Logger("logging.conf", "BATH", "INFO").get_logger()

vault = Vault(
    VAULT_TOKEN, logger=logger, twitter_path=app.config["VAULT_TWITTER_PATH"]
)

keys = vault.get_twitter_keys()

# create IPC queues
data_queue = Queue()
signal_queue = Queue()

# init storage
tub = Storage(
    30000, logger=logger, data_queue=data_queue, signal_queue=signal_queue
)
tub_worker = Process(target=tub.run, args=())
tub_worker.daemon = True
tub_worker.start()

# connect stream
faucet = Stream(keys, 2, logger=logger, data_queue=data_queue)
faucet_worker = Process(target=faucet.run, args=())
faucet_worker.daemon = True
faucet_worker.start()

# init socket server
drain = Socket(
    app,
    logger=logger,
    message_queue=app.config["QUEUE_ADDR"],
    signal_queue=signal_queue,
)

drain_worker = Process(
    target=drain.run,
    args=(app,),
    kwargs={
        "host": app.config["SOCKET_HOST"],
        "port": app.config["SOCKET_PORT"],
        "keyfile": app.config["KEYFILE"],
        "certfile": app.config["CERTFILE"],
    },
)
drain_worker.daemon = True
drain_worker.start()

# hold main thread open indefinitely
while True:
    signal.pause()
