import os
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from bath.tub.model_tweet import Tweet
from bath.tub.model_place import Place
from bath.tub.model_hashtag import Hashtag
from bath.tub.model_mention import Mention
from bath.tub.tweets_hashtags import TweetsToHashtags


class DBController:
    def __init__(self, **kwargs):
        self.logger = kwargs.get("logger", None)
        self.db_string = self._get_db_string()
        self.env = os.getenv("BATH_ENV", "production")
        if self.env == "production":
            self.engine = create_engine(
                self.db_string, connect_args={"sslmode": "require"}
            )
        else:
            self.engine = create_engine(self.db_string)
        self.db_string = None
        self.session = None

        if self.logger:
            self.logger.info("DB connection established")

    def _get_db_string(self):
        db_string = os.getenv("SQLALCHEMY_URL")
        return db_string

    def get_session(self):
        if self.session is None:
            self.Session = sessionmaker(bind=self.engine)
            self.session = self.Session()
        return self.session
