import sqlalchemy as sa
from bath.tub.base import Base
from bath.tub.model_tweet import Tweet
from bath.tub.model_hashtag import Hashtag


class TweetsToHashtags(Base):
    __tablename__ = "tweets_to_hashtags"
    id = sa.Column(sa.Integer, primary_key=True)
    tweet_id = sa.Column(sa.String(30), sa.ForeignKey("tweets.tweet_id"))
    hashtag_id = sa.Column(
        sa.String(140), sa.ForeignKey("hashtags.hashtag_id")
    )
