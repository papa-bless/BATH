import sqlalchemy as sa
from geoalchemy2.types import Geography
from bath.tub.base import Base


class Place(Base):
    __tablename__ = "places"

    id = sa.Column(sa.Integer, primary_key=True)
    place_id = sa.Column(sa.String(20))
    url = sa.Column(sa.String(60))
    place_type = sa.Column(sa.String(30))
    name = sa.Column(sa.String(100))
    full_name = sa.Column(sa.String(100))
    country_code = sa.Column(sa.String(10))
    country = sa.Column(sa.String(100))
    bounding_box = sa.Column(Geography(geometry_type="POLYGON", srid=4326))
    tweet_count = sa.Column(sa.Integer, nullable=True)
    avg_sentiment = sa.Column(sa.Float, nullable=True)

    def __repr__(self):
        return "<Place(place_id='{}', name='{}')>".format(
            self.place_id, self.name
        )
