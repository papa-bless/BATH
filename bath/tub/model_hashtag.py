import sqlalchemy as sa
from bath.tub.base import Base


class Hashtag(Base):
    __tablename__ = "hashtags"

    id = sa.Column(sa.Integer, primary_key=True)
    hashtag_id = sa.Column(sa.String(140))

    def __repr__(self):
        return "<Hashtag(id='{}', hashtag_id='{}')>".format(
            self.id, self.hashtag_id
        )
