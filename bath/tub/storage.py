import json
import os
import pickle

import util.common as common
from bath.tub.db_controller import DBController
from bath.tub.model_tweet import Tweet
from flask_socketio import SocketIO
from geoalchemy2.shape import to_shape
from google.cloud import exceptions, storage
from util.array import CircularArray
from util.scheduler import Scheduler


class Storage:
    def __init__(self, size, **kwargs):
        self.size = size
        self.logger = kwargs.get("logger", None)

        self.data_queue = kwargs.get("data_queue", None)
        self.signal_queue = kwargs.get("signal_queue", None)

        self.db = DBController()
        self.session = self.db.get_session()

        self.data = self.load()
        self.socketio = SocketIO(message_queue="redis://redis")
        self.logger.info("Storage initialized")

    def run(self):
        self.scheduler = Scheduler()
        self.scheduler.add(1, self.process_data_queue)
        self.scheduler.run()
        while True:
            if not self.signal_queue.empty():
                sid = self.signal_queue.get()
                self.logger.debug("sid ({}) recvd".format(sid))
                payload = [
                    x for x in self.data.__dict__["array"] if x is not None
                ]
                self.socketio.emit("history", json.dumps(payload), sid)
                self.logger.debug("history emitted")

    def process_data_queue(self):
        self.logger.debug("checking queue")
        while not self.data_queue.empty():
            self.insert(self.data_queue.get())
            self.logger.debug("local cache updated")

    def empty(self):
        return len(self.data) == 0

    def insert(self, item):
        self.data.insert(item)

    def load(self):
        memory = CircularArray(self.size)

        q = (
            self.session.query(Tweet)
            .order_by(Tweet.id.desc())
            .limit(self.size)
        )

        for record in q:
            coords = to_shape(record.coordinates)
            item = {}
            item["lat"] = coords.y
            item["lon"] = coords.x
            item["sentiment"] = record.sentiment
            memory.insert(item)

        self.logger.info("{} objects loaded".format(len(memory)))

        return memory
