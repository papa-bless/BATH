import sqlalchemy as sa
from geoalchemy2.types import Geography
from bath.tub.base import Base


class Tweet(Base):
    __tablename__ = "tweets"
    __table_args__ = (sa.UniqueConstraint("tweet_id"),)

    id = sa.Column(sa.Integer, primary_key=True)
    created_at = sa.Column(sa.DateTime())
    tweet_id = sa.Column(sa.String(30))
    text = sa.Column(sa.Unicode(500))
    user_id = sa.Column(sa.String(30))
    in_reply_to_status_id_str = sa.Column(sa.String(30), nullable=True)
    in_reply_to_user_id_str = sa.Column(sa.String(30), nullable=True)
    in_reply_to_screen_name = sa.Column(sa.String(50), nullable=True)
    coordinates = sa.Column(
        Geography(geometry_type="POINT", srid=4326), nullable=True
    )
    place_id = sa.Column(sa.String(20), nullable=True)
    quoted_status_id_str = sa.Column(sa.String(30), nullable=True)
    is_quote_status = sa.Column(sa.Boolean(), default=False)
    quoted_status = sa.Column(sa.Unicode(200), nullable=True)
    retweeted_status = sa.Column(sa.Unicode(200), nullable=True)
    quote_count = sa.Column(sa.Integer, nullable=True)
    reply_count = sa.Column(sa.Integer, nullable=True)
    favorited = sa.Column(sa.Boolean, nullable=True)
    retweeted = sa.Column(sa.Boolean, default=False)
    favorite_count = sa.Column(sa.Integer, nullable=True)
    sentiment = sa.Column(sa.Float(), nullable=True)

    mentions = sa.orm.relationship("Mention", back_populates="tweet")
    hashtags = sa.orm.relationship("Hashtag", secondary="tweets_to_hashtags")

    def __repr__(self):
        return "<Tweet(id='{}', tweet_id='{}')>".format(self.id, self.tweet_id)
