import sqlalchemy as sa
from bath.tub.base import Base


class Mention(Base):
    __tablename__ = "mentions"

    id = sa.Column(sa.Integer, primary_key=True)
    user_id = sa.Column(sa.String(30))
    tweet_id = sa.Column(sa.String(30), sa.ForeignKey("tweets.tweet_id"))
    tweet = sa.orm.relationship("Tweet", back_populates="mentions")

    def __repr__(self):
        return "<Mention(user_id='{}')>".format(self.user_id)
