import json
import os
import ssl

from flask import request
from flask_socketio import SocketIO
from geoalchemy2.shape import to_shape

from bath.tub.model_hashtag import Hashtag
from bath.tub.model_mention import Mention
from bath.tub.model_place import Place
from bath.tub.model_tweet import Tweet
from bath.tub.db_controller import DBController
from bath.tub.tweets_hashtags import TweetsToHashtags


class Socket(SocketIO):
    def __init__(self, app, **kwargs):
        self.logger = kwargs.get("logger", None)
        self.message_queue = kwargs.get("message_queue", None)
        self.signal_queue = kwargs.get("signal_queue", None)
        self.history = set()

        self.db = DBController()

        super().__init__(
            app, message_queue=self.message_queue, cors_allowed_origins="*"
        )

        self.setup_events()

    def run(self, *args, **kwargs):
        if kwargs.get("keyfile", None) is not None:
            super().run(*args, **kwargs)
        else:
            super().run(
                *args, host=kwargs.get("host"), port=kwargs.get("port")
            )

    def setup_events(self):
        @self.on("connect")
        def handle_connect():
            self.logger.info(
                "client connected: {} - {}".format(
                    request.remote_addr, request.sid
                )
            )

            self.signal_queue.put(request.sid)

            self.logger.debug(
                "signal request.sid ({}) to tub".format(request.sid)
            )

        @self.on("disconnect")
        def handle_disconnect():
            self.logger.info(
                "client disconnected: {} - {}".format(
                    request.remote_addr, request.sid
                )
            )
