import os
import re
import time

import pandas as pd
import catboost
from textblob import TextBlob
from sklearn import model_selection

import util.common as common


class Model:
    def __init__(self, **kwargs):
        self.logger = kwargs.get("logger", None)
        self.model_data = common.repo_root("data/storage/catboost_model.dump")
        self.load_model()
        self.logger.info("Model initialized")

    def load_model(self):
        if os.path.exists(self.model_data):
            self.logger.info("trained model found")
            self.model = catboost.CatBoostClassifier()
            self.model.load_model(self.model_data)
            self.logger.info("trained model loaded")

        else:
            self.train()

    def clean_tweet(self, str):
        return " ".join(
            re.sub(
                "(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])|(\w+:\/\/\S+)", " ", str
            ).split()
        )

    def score(self, str):
        cleaned = self.clean_tweet(str)
        blob = TextBlob(cleaned)

        return self.model.predict(
            [
                len(blob.words),
                len(blob.word_counts),
                blob.sentiment[0],
                blob.sentiment[1],
            ]
        )

    def train(self):

        training_data = common.repo_root("data/training_tweets_kaggle.csv")
        tweets = pd.read_csv(
            filepath_or_buffer=training_data, encoding="latin-1",
        )

        tweets = tweets.sample(frac=1)
        sentiments = []
        clean_tweets_linear = []
        clean_tweets_linear_target = []
        clean_tweets = []
        start = time.time()

        for index, tweet in tweets.head(10000).iterrows():
            cleaned_tweet = self.clean_tweet(tweet.text)
            blob_tweet = TextBlob(cleaned_tweet)
            clean_tweets_linear.insert(
                index,
                [
                    len(blob_tweet.words),
                    len(blob_tweet.word_counts),
                    blob_tweet.sentiment[0],
                    blob_tweet.sentiment[1],
                ],
            )
            clean_tweets_linear_target.insert(index, tweet.target)
            clean_tweets.insert(index, cleaned_tweet)
            sentiments.insert(index, TextBlob(cleaned_tweet).sentiment)

        x_train, x_test, y_train, y_test = model_selection.train_test_split(
            clean_tweets_linear,
            clean_tweets_linear_target,
            test_size=0.20,
            random_state=42,
        )
        model = catboost.CatBoostClassifier(
            iterations=1000, learning_rate=0.001,
        )
        model.fit(x_train, y_train, eval_set=(x_test, y_test))

        self.logger.info("saving trained model to {}".format(self.model_data))
        model.save_model(self.model_data)
        self.model = model
        finish = time.time()
        self.logger.info(
            "Training complete. Elapsed time: {}".format(finish - start)
        )
