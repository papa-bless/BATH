import json
import sys
import threading
import time
from queue import Queue

import tweepy
from flask_socketio import SocketIO
from requests.exceptions import ConnectionError
from urllib3.exceptions import ProtocolError, ReadTimeoutError

from bath.toaster.model import Model
from bath.tub.db_controller import DBController
from bath.tub.model_hashtag import Hashtag
from bath.tub.model_mention import Mention
from bath.tub.model_place import Place
from bath.tub.model_tweet import Tweet
from bath.tub.tweets_hashtags import TweetsToHashtags


class Stream:
    def __init__(self, keys, num_threads, **kwargs):
        self.keys = keys
        self.num_threads = num_threads
        self.logger = kwargs.get("logger", None)
        self.data_queue = kwargs.get("data_queue", None)
        self.timeout_restarts = 0
        self.protocol_error_restarts = 0

        self.auth = tweepy.OAuthHandler(
            self.keys["twitter_consumer_key"],
            self.keys["twitter_consumer_secret"],
        )
        self.auth.set_access_token(
            self.keys["twitter_access_token_key"],
            self.keys["twitter_access_token_secret"],
        )

        self.listener = StreamListener(
            self.num_threads, self.logger, self.data_queue
        )
        self.logger.info("StreamListener initialized.")

    def run(self):
        self.logger.info("starting Twitter stream...")
        self.stream = tweepy.Stream(self.auth, self.listener, timeout=300.0)
        self.logger.info("initialized, connecting...")
        while True:
            if not self.stream.running:
                try:
                    self.stream.filter(
                        languages=["en"],
                        # Lower 48
                        # Alaska
                        # Hawaii
                        locations=[
                            -127.880859,
                            24.766785,
                            -66.708984,
                            49.496675,
                            -170.244141,
                            53.278353,
                            -140.625000,
                            71.691293,
                            -160.971680,
                            18.458768,
                            -154.215088,
                            22.847071,
                        ],
                        is_async=False,
                        stall_warnings=True,
                    )
                except ReadTimeoutError:
                    self.logger.warning("ReadTimeoutError caught")

                except ProtocolError:
                    self.logger.warning("ProtocolError caught")

                except ConnectionError:
                    self.logger.warning("ConnectionError caught")

                except KeyboardInterrupt:
                    self.logger.info("Keyboard interrupt received, exiting")
                    sys.exit(0)

                finally:
                    self.stream.disconnect()
                    time.sleep(60)


class StreamListener(tweepy.StreamListener):
    def __init__(self, num_threads, logger, data_queue):
        super(StreamListener, self).__init__()
        self.num_threads = num_threads
        self.logger = logger
        self.data_queue = data_queue
        self.model = Model(logger=logger)

        self.threads = list()
        self.tweetQueue = Queue()

        self.socketio = SocketIO(message_queue="redis://redis")

        self.db = DBController()

        for i in range(self.num_threads):
            t = threading.Thread(target=self.process_queue, args=(i,))
            self.logger.debug("thread-{} created".format(i))
            t.daemon = True
            t.start()
            self.logger.debug("thread-{} started".format(i + 1))
            self.threads.append(t)

    def process_coordinate_tweet(self, status, thread_id):
        session = self.db.get_session()
        response = {
            "lat": status.coordinates["coordinates"][1],
            "lon": status.coordinates["coordinates"][0],
            "sentiment": self.model.score(status.text),
        }

        self.data_queue.put(response)
        self.socketio.emit("tweet-coordinates", json.dumps(response))

        tweet_q = (
            session.query(Tweet).filter_by(tweet_id=status.id_str).first()
        )
        if tweet_q is not None:
            self.logger.debug(
                "duplicate tweet found {}".format(tweet_q.tweet_id)
            )
            return

        if hasattr(status, "extended_tweet"):
            text = status.extended_tweet["full_text"]
        else:
            text = status.text

        if status.is_quote_status:
            is_quote_status = True
            quoted_status_id_str = status.quoted_status_id_str
            quoted_status = status.quoted_status.text
        else:
            is_quote_status = False
            quoted_status_id_str = None
            quoted_status = None

        if hasattr(status, "retweeted_status"):
            retweeted = True
        else:
            retweeted = False

        if status.place is not None:
            place_id = status.place.id
        else:
            place_id = None

        t = Tweet(
            created_at=status.created_at,
            tweet_id=status.id_str,
            text=text,
            user_id=status.author.id_str,
            in_reply_to_status_id_str=status.in_reply_to_status_id_str,
            in_reply_to_user_id_str=status.in_reply_to_user_id_str,
            in_reply_to_screen_name=status.in_reply_to_screen_name,
            coordinates="POINT({} {})".format(
                response["lon"], response["lat"]
            ),
            place_id=place_id,
            is_quote_status=is_quote_status,
            quoted_status_id_str=quoted_status_id_str,
            quoted_status=quoted_status,
            quote_count=status.quote_count,
            reply_count=status.reply_count,
            favorited=status.favorited,
            retweeted=retweeted,
            favorite_count=status.favorite_count,
            sentiment=self.model.score(text),
        )

        for hashtag in status.entities["hashtags"]:
            h = (
                session.query(Hashtag)
                .filter_by(hashtag_id=hashtag["text"])
                .first()
            )
            if h is None:
                h = Hashtag(hashtag_id=hashtag["text"])
                session.add(h)
            t.hashtags.append(h)
        session.add(t)
        session.commit()

        self.logger.debug("insert {} into tweets".format(t))
        # self.logger.info(status.entities["hashtags"])
        # add hashtags
        # add mentions

    def process_place(self, status):
        session = self.db.get_session()
        if hasattr(status, "extended_tweet"):
            text = status.extended_tweet["full_text"]
        else:
            text = status.text

        place_id = status.place.id
        sentiment = self.model.score(text)

        place_q = session.query(Place).filter_by(place_id=place_id).scalar()
        if place_q is not None:
            tweet_count = place_q.tweet_count
            avg_sentiment = place_q.avg_sentiment
            new_avg_sentiment = (
                avg_sentiment + (sentiment - avg_sentiment) / tweet_count
            )

            place_q.tweet_count = tweet_count + 1
            place_q.avg_sentiment = new_avg_sentiment
            session.commit()

        else:

            bounding_box = "POLYGON(({} {}, {} {}, {} {}, {} {}, {} {}))".format(
                status.place.bounding_box.coordinates[0][0][0],
                status.place.bounding_box.coordinates[0][0][1],
                status.place.bounding_box.coordinates[0][1][0],
                status.place.bounding_box.coordinates[0][1][1],
                status.place.bounding_box.coordinates[0][2][0],
                status.place.bounding_box.coordinates[0][2][1],
                status.place.bounding_box.coordinates[0][3][0],
                status.place.bounding_box.coordinates[0][3][1],
                status.place.bounding_box.coordinates[0][0][0],
                status.place.bounding_box.coordinates[0][0][1],
            )
            p = Place(
                place_id=status.place.id,
                url=status.place.url,
                place_type=status.place.place_type,
                name=status.place.name,
                full_name=status.place.full_name,
                country_code=status.place.country_code,
                bounding_box=bounding_box,
                tweet_count=1,
                avg_sentiment=self.model.score(text),
            )
            self.logger.debug("insert {} into places".format(p))
            session.add(p)
            session.commit()

    def process_queue(self, id):
        while True:
            status = self.tweetQueue.get()

            if hasattr(status, "retweeted_status"):
                self.logger.warning("RETWEETED STATUS")
            if status.coordinates is not None:
                self.process_coordinate_tweet(status, id)
                self.tweetQueue.task_done()
                continue

            elif status.place is not None:
                self.process_place(status)
                self.tweetQueue.task_done()
                continue
            self.tweetQueue.task_done()

    def keep_alive(self):
        self.logger.info("KEEP_ALIVE received")

    def on_connect(self):
        self.logger.info("connected to streaming API")

    def on_disconnect(self, notice):
        self.logger.warning("DISCONNECT received: {}".format(notice))
        self.logger.warning(
            """SEE: https://developer.twitter.com/en/docs/tweets/filter-
            realtime/guides/streaming-message-types"""
        )
        self.logger.info("Disconnected")
        return False

    def on_error(self, status_code):
        self.logger.error("ERROR: {}".format(status_code))
        if status_code == 420:
            return False

    def on_limit(self, track):
        self.logger.warning("ON_LIMIT received: {}".format(track))

    def on_status(self, status):
        self.logger.debug("New tweet: {}".format(status.id))
        self.tweetQueue.put(status)

    def on_warning(self, notice):
        self.logger.warning("WARNING received: {}".format(notice))
