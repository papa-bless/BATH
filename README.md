# B.A.T.H.

version 0.5.3

## Overview
```mermaid
sequenceDiagram
  Participant B as BATH
  Participant V as Vault
  Participant T as Twitter
  Participant S as TOASTER
  Participant R as React Clients
  Note right of B: request API Keys
  B->>V: secrets.kv.v2.twitter
  V-->>B: {API keys}
  Note over V: establish stream
  B->>T: stream.filter(languages=['en'])
  loop on new tweet
    rect rgba(198,246,213)
      T-->>B: {matched tweet}
    end
    rect rgb(154,230,180)
      B->>S: score this tweet
      S-->>B: {score: 0.0217}
    end
    rect rgb(104,211,145)
      B->>R: {lat, lon, score}
    end
  end
```

## Configuration
All this needs to run currently is a valid vault token in `.env` in the repo root.

Ex `.env`:
```
VAULT_TOKEN=xxx
BATH_ENV=development
SQLALCHEMY_URL=driver://user:password@host/database-name
```
BATH will use this to request any further credentials needed from the Vault server.

## Docker Setup
```bash
poetry export -f requirements.txt > requirements.txt
docker-compose build && docker-compose up -d redis web && docker-compose logs -f web
# or simply
make up
make down
```

## Client Usage
```bash
python -m bath-client <staging|local|prod>
```

## Development

To get started, clone the repo and cd into it:

```bash
git clone git@gitlab.com:papa-bless/BATH.git
cd BATH
```

Install dependencies with **poetry** and setup pre-commit hooks:

```bash
pip install --user poetry
poetry install
poetry shell
pre-commit install
# write a bunch of great code...
exit
```

Before running code ensure you are in a virtual environment:
```bash
poetry shell
```

### Testing
```bash
make test
```

