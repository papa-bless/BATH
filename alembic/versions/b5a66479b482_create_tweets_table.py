"""create tweets table

Revision ID: b5a66479b482
Revises:
Create Date: 2020-01-27 21:01:15.434558

"""
from alembic import op
import sqlalchemy as sa
from geoalchemy2.types import Geography

# revision identifiers, used by Alembic.
revision = "b5a66479b482"
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "tweets",
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column("tweet_id", sa.String(30)),
        sa.Column("text", sa.Unicode(500)),
        sa.Column("user_id", sa.String(30)),
        sa.Column("in_reply_to_status_id_str", sa.String(30), nullable=True),
        sa.Column("in_reply_to_user_id_str", sa.String(30), nullable=True),
        sa.Column("in_reply_to_screen_name", sa.String(50), nullable=True),
        sa.Column(
            "coordinates",
            Geography(geometry_type="POINT", srid=4326),
            nullable=True,
        ),
        sa.Column("place_id", sa.String(20), nullable=True),
        sa.Column("quoted_status_id_str", sa.String(30), nullable=True),
        sa.Column("is_quote_status", sa.Boolean(), default=False),
        sa.Column("quoted_status", sa.Unicode(200), nullable=True),
        sa.Column("retweeted_status", sa.Unicode(200), nullable=True),
        sa.Column("quote_count", sa.Integer, nullable=True),
        sa.Column("reply_count", sa.Integer, nullable=True),
        sa.Column("favorited", sa.Boolean, nullable=True),
        sa.Column("retweeted", sa.Boolean, default=False),
        sa.Column("favorite_count", sa.Integer, nullable=True),
        sa.UniqueConstraint("tweet_id"),
    )


def downgrade():
    op.drop_table("tweets")
