"""add created_at column

Revision ID: a590938a246d
Revises: bffd60f1b85b
Create Date: 2020-03-10 16:33:37.053143

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "a590938a246d"
down_revision = "bffd60f1b85b"
branch_labels = None
depends_on = None


def upgrade():
    op.add_column("tweets", sa.Column("created_at", sa.DateTime()))


def downgrade():
    op.drop_column("tweets", "created_at")
