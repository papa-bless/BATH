"""create mentions table

Revision ID: 4b6f6ffec998
Revises: 5afff64337b8
Create Date: 2020-02-04 23:43:32.745553

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "4b6f6ffec998"
down_revision = "5afff64337b8"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "mentions",
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column("tweet_id", sa.String(30)),
        sa.Column("user_id", sa.String(30)),
    )


def downgrade():
    op.drop_table("mentions")
