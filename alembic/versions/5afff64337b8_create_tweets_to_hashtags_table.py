"""create tweets_to_hashtags table

Revision ID: 5afff64337b8
Revises: 8b927fdb44cd
Create Date: 2020-01-27 21:53:38.867658

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "5afff64337b8"
down_revision = "35d3eb381e0a"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "tweets_to_hashtags",
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column("tweet_id", sa.String(30), sa.ForeignKey("tweets.tweet_id")),
        sa.Column(
            "hashtag_id", sa.String(140), sa.ForeignKey("hashtags.hashtag_id")
        ),
    )


def downgrade():
    op.drop_table("tweets_to_hashtags")
