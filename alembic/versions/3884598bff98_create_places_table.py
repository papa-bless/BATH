"""create places table

Revision ID: 3884598bff98
Revises: b5a66479b482
Create Date: 2020-01-27 21:03:22.298827

"""
from alembic import op
import sqlalchemy as sa
from geoalchemy2.types import Geography

# revision identifiers, used by Alembic.
revision = "3884598bff98"
down_revision = "b5a66479b482"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "places",
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column("place_id", sa.String(20)),
        sa.Column("url", sa.String(60)),
        sa.Column("place_type", sa.String(30)),
        sa.Column("name", sa.String(100)),
        sa.Column("full_name", sa.String(100)),
        sa.Column("country_code", sa.String(10)),
        sa.Column("country", sa.String(100)),
        sa.Column(
            "bounding_box", Geography(geometry_type="POLYGON", srid=4326)
        ),
        sa.Column("tweet_count", sa.Integer, nullable=True),
        sa.Column("avg_sentiment", sa.Float, nullable=True),
    )


def downgrade():
    op.drop_table("places")
