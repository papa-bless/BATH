"""add limit_tweets trigger

Revision ID: e4ec3366c816
Revises: 4b6f6ffec998
Create Date: 2020-02-20 23:04:39.973427

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "e4ec3366c816"
down_revision = "4b6f6ffec998"
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        "CREATE OR REPLACE FUNCTION limit_records()\n"
        "RETURNS TRIGGER AS\n"
        "$body$\n"
        "DECLARE\n"
        "tab text;\n"
        "keyfld text;\n"
        "nritems INTEGER;\n"
        "rnd DOUBLE PRECISION;\n"
        "BEGIN\n"
        "tab := TG_ARGV[0];\n"
        "keyfld := TG_ARGV[1];\n"
        "nritems := TG_ARGV[2]; \n"
        "rnd := TG_ARGV[3];\n"
        "IF random() < rnd\n"
        "THEN \n"
        "EXECUTE(format('DELETE FROM %s WHERE %s < (SELECT %s FROM %s ORDER BY %s DESC LIMIT 1 OFFSET %s)', tab, keyfld, keyfld, tab, keyfld, nritems));\n"
        "END IF;\n"
        "RETURN NULL;\n"
        "END;\n"
        "$body$\n"
        "LANGUAGE plpgsql;\n"
        "CREATE TRIGGER limit_tweets\n"
        "AFTER INSERT ON tweets\n"
        "FOR EACH STATEMENT EXECUTE PROCEDURE limit_records('tweets', 'id', 1000000, 0.1);\n"
    )


def downgrade():
    pass
