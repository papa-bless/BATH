"""add sentiment column to tweets

Revision ID: bffd60f1b85b
Revises: e4ec3366c816
Create Date: 2020-02-23 20:43:28.216803

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "bffd60f1b85b"
down_revision = "e4ec3366c816"
branch_labels = None
depends_on = None


def upgrade():
    op.add_column("tweets", sa.Column("sentiment", sa.Float(), nullable=True))


def downgrade():
    op.drop_column("tweets", "sentiment")
