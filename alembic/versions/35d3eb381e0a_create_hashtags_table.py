"""create hashtags table

Revision ID: 35d3eb381e0a
Revises: 22e0e51505ab
Create Date: 2020-01-28 22:47:40.508693

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "35d3eb381e0a"
down_revision = "3884598bff98"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "hashtags",
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column("hashtag_id", sa.String(140)),
        sa.UniqueConstraint("hashtag_id"),
    )


def downgrade():
    op.drop_table("hashtags")
