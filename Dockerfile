FROM python:3.7.4-stretch

WORKDIR /app

COPY ./requirements.txt /app

RUN pip --disable-pip-version-check install -r requirements.txt

RUN python -m textblob.download_corpora

COPY . /app

CMD ["python", "-m", "bath"]
